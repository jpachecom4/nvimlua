return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use 'chriskempson/base16-vim'
    use 'preservim/nerdtree'
    use 'jreybert/vimagit'
    use 'lukesmithxyz/vimling'
    use 'vimwiki/vimwiki'
    use 'vim-airline/vim-airline'
    use 'vim-airline/vim-airline-themes'
    use 'tpope/vim-commentary'
    use 'fedepujol/move.nvim'
    use 'lilydjwg/colorizer'
    use 'tpope/vim-surround'
    use 'Yggdroot/indentLine'
    use 'vim-scripts/AutocomplPop'
    use 'vim-scripts/tComment'
    use 'markonm/traces.vim'
    use 'jiangmiao/auto-pairs'
    use 'neovim/nvim-lspconfig'
    use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
    }
end)
