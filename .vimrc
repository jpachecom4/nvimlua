" vimrc config for  vim 
" javier pacheco

set number
set relativenumber
set mouse=a
syntax enable
set clipboard=unnamedplus
set termguicolors
set title
set go=a
set nohlsearch
set noshowmode
set noruler
set laststatus=0
set noshowcmd


"plugins
call plug#begin()
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'jreybert/vimagit'
Plug 'lukesmithxyz/vimling'
Plug 'vimwiki/vimwiki'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'matze/vim-move'
Plug 'lilydjwg/colorizer'
Plug 'gruvbox-community/gruvbox'
Plug 'tpope/vim-surround'
Plug 'Yggdroot/indentLine'
Plug 'vim-scripts/AutocomplPop'
Plug 'vim-scripts/tComment'
Plug 'markonm/traces.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'unblevable/quick-scope'
Plug 'neovim/nvim-lspconfig'
Plug 'Yggdroot/indentLine'
call plug#end()

colorscheme gruvbox
